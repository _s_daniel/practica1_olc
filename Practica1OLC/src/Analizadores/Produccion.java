/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analizadores;

import java.util.LinkedList;

/**
 *
 * @author da9ni5el
 */
public class Produccion{
    protected String yname;
    protected int xtoken;
    protected int y;
    protected int x;
    protected LinkedList<Object> producciones;

    public Produccion() {
    }

    public Produccion(String yname, int xtoken, int y, int x, LinkedList<Object> producciones) {
        this.yname = yname;
        this.xtoken = xtoken;
        this.y = y;
        this.x = x;
        this.producciones = producciones;
    }

    public String getYname() {
        return yname;
    }

    public void setYname(String yname) {
        this.yname = yname;
    }

    public int getXtoken() {
        return xtoken;
    }

    public void setXtoken(int xtoken) {
        this.xtoken = xtoken;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public LinkedList<Object> getProducciones() {
        return producciones;
    }

    public void setProducciones(LinkedList<Object> producciones) {
        this.producciones = producciones;
    }
    
    public boolean igualQue(Produccion q, int type) {
        return this.getX() == q.getX() && this.getY() == q.getY();
    }
    
    public String escribirObjeto() {
        StringBuffer sb = new StringBuffer();
        
        sb.append("Produccion: ");
        if(this.producciones == null)
            return "epsilon";
        
        if(!this.producciones.isEmpty()){
            for(Object s: producciones){
                sb.append(s.toString()).append(" ");
            }
        }
        
        return sb.toString();
    }

}
