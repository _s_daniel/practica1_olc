/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analizadores;

import AST.Nodo;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author da9ni5el
 *
 * Java trabaja filaxcolumna
 */
public class Parser {

    TablaAS tabla;
    LinkedList<Token> entrada;
    Stack<Object> pila;
    Nodo raiz;
    private int ctoken;

    public Parser(LinkedList<Token> entrada) {
        this.entrada = entrada;
        this.pila = new Stack<>();
        tabla = new TablaAS();
        ctoken = -1;
        raiz = new Nodo();
        raiz.setEtiqueta("inicio");
    }

    public Nodo getRaiz() {
        return raiz;
    }

    public void parse() {
        pila.push("0");
        pila.push(raiz);
        pila.push("INICIO");
        Nodo ntemp = null; //Siempre sera la ultima produccion que se derivo.
        Object temp;

        int token = next_token();
        do {
            temp = pila.peek();
            if (isNumeric(temp.toString()) || temp.equals("0")) {
                if (temp.equals("200")) {
                    temp = String.valueOf(token);
                }

                if (temp.equals(String.valueOf(token))) { //Aqui habria problema??? 
                    pila.pop(); //Sacando terminal
                    switch (token) {
                        case 2: // id

                            if (ntemp.getEtiqueta().equals("lex")) {
                                if (ntemp.getNombre() == null) {
                                    ntemp.setNombre(entrada.get(ctoken).getLexema());
                                } else {
                                    ntemp.setValor(entrada.get(ctoken).getLexema());
                                }
                                break;
                            }

                            ntemp.setValor(entrada.get(ctoken).getLexema());
                            ntemp.setTipo(token);
                            break;
                        case 3: // numero
                            ntemp.setValor(entrada.get(ctoken).getLexema());
                            ntemp.setTipo(token);
                            break;
                        case 6: // cadena
                            ntemp.setValor(entrada.get(ctoken).getLexema());
                            ntemp.setTipo(token);
                            break;
                        default:
                            //Simbolos
                            //intentando arreglar erorres con los simbolos
                            if (!ntemp.getEtiqueta().equals("t") && !ntemp.getEtiqueta().equals("te")) {
                                System.out.println("Se ignora el siguiente caracter: " + entrada.get(ctoken).getLexema());
                                break;
                            }
                            if (token >= 32 && token <= 125) {
                                if (ntemp.getValor() != "") {
                                    break;
                                }

                                int tktemp = getSymbolToken(entrada.get(ctoken).getLexema());
                                ntemp.setTipo(tktemp);
                                ntemp.setValor(entrada.get(ctoken).getLexema());
//                                ntemp.getHijos().get(0).setEtiqueta(String.valueOf(tktemp));
//                                ntemp.getHijos().get(0).setValor(entrada.get(ctoken).getLexema());
                            } else {
//                                System.out.println("Se ignora el siguiente caracter: " + entrada.get(ctoken).getLexema());
                            }
                            break;
                    }
                    token = next_token(); //Se avanza en la entrada
                } else {
                    //error
                    System.out.println("Error 1");
                    System.out.println("Se recibe: " + String.valueOf(token));
                    System.out.println("Y se esperaba: " + temp.toString());
                    return;
                }
            } else {
                Produccion p = tabla.tabla_as.buscar(temp, token);
                if (p != null) {
                    pila.pop();
                    ntemp = (Nodo) pila.pop();
                    //En este switch elijo que nodos necesito crear
                    switch (temp.toString().toLowerCase()) {
                        case "inicio":
                            // { CUERPO }
                            ntemp.agregarHijo(new Nodo("cuerpo", ""));

                            pila.push(p.producciones.get(2)); // }
                            pila.push(ntemp.getHijos().get(0)); // nodo Cuerpo
                            pila.push(p.producciones.get(1)); // cuerpo
                            pila.push(p.producciones.get(0)); // {
                            //
                            break;
                        case "cuerpo":
                            //DEC %%%% LEX
                            ntemp.agregarHijo(new Nodo("dec", ""));
                            ntemp.agregarHijo(new Nodo("lex", ""));

                            pila.push(ntemp.getHijos().get(1)); // nodo lex
                            pila.push(p.producciones.get(5)); // lex
                            pila.push(p.producciones.get(4)); // %
                            pila.push(p.producciones.get(3));
                            pila.push(p.producciones.get(2));
                            pila.push(p.producciones.get(1)); // %
                            pila.push(ntemp.getHijos().get(0)); // nodo dec
                            pila.push(p.producciones.get(0)); // dec
                            break;
                        case "dec":
                            //DEC::= CONJ DECR | ER DECR
                            ntemp.agregarHijo(new Nodo(p.producciones.get(0).toString().toLowerCase(), ""));
                            ntemp.agregarHijo(new Nodo("decr", ""));

                            pila.push(ntemp.getHijos().get(1)); // nodo decr
                            pila.push(p.producciones.get(1)); // decr
                            pila.push(ntemp.getHijos().get(0)); // nodo conj | er
                            pila.push(p.producciones.get(0));// conj | er
                            break;
                        case "decr":
                            // DEC | 3
                            if (p.getProducciones().size() == 0) {
                                continue;
                            }
                            ntemp.agregarHijo(new Nodo("dec", ""));

                            pila.push(ntemp.getHijos().get(0));
                            pila.push(p.producciones.get(0));
                            break;
                        case "conj":
                            // conj: id -> CONJC;
//                            ntemp.agregarHijo(new Nodo(p.producciones.get(2).toString(), ""));
                            ntemp.agregarHijo(new Nodo("conjc", ""));

                            pila.push(p.producciones.get(5));
                            pila.push(ntemp.getHijos().get(0)); // nodo conjc
                            pila.push(p.producciones.get(4)); // conjc
                            pila.push(p.producciones.get(3)); // ->
                            pila.push(p.producciones.get(2)); // id
                            pila.push(p.producciones.get(1)); // :
                            pila.push(p.producciones.get(0)); // conj
                            break;
                        case "conjc":
                            //T OP
                            ntemp.agregarHijo(new Nodo("t", ""));
                            ntemp.agregarHijo(new Nodo("op", ""));

                            pila.push(ntemp.getHijos().get(1)); // nodo op
                            pila.push(p.producciones.get(1)); // op
                            pila.push(ntemp.getHijos().get(0)); // nodo t
                            pila.push(p.producciones.get(0)); // t
                            break;
                        case "op":
                            // ~T | LIST
                            if (p.producciones.size() == 2) {
                                ntemp.agregarHijo(new Nodo("t", ""));

                                pila.push(ntemp.getHijos().get(0)); // nodo T
                                pila.push(p.producciones.get(1)); // T
                                pila.push(p.producciones.get(0)); // ~
                            } else {
                                ntemp.agregarHijo(new Nodo("list", ""));

                                pila.push(ntemp.getHijos().get(0)); //  nodo list
                                pila.push(p.producciones.get(0)); // list
                            }
                            break;
                        case "list":
                            // , T LISTR
                            ntemp.agregarHijo(new Nodo("t", ""));
                            ntemp.agregarHijo(new Nodo("listr", ""));

                            pila.push(ntemp.getHijos().get(1)); // nodo listr
                            pila.push(p.producciones.get(2)); // listr
                            pila.push(ntemp.getHijos().get(0)); // nodo t
                            pila.push(p.producciones.get(1)); // t
                            pila.push(p.producciones.get(0)); // ,
                            break;
                        case "listr":
                            // LIST | 3
                            if (p.getProducciones().size() == 0) {
                                continue;
                            }

                            ntemp.agregarHijo(new Nodo("list", ""));

                            pila.push(ntemp.getHijos().get(0));// nodo list
                            pila.push(p.producciones.get(0)); // list
                            break;
                        case "t":
                            //Aqui no guardo nada en el arbol.
//                            ntemp.agregarHijo(new Nodo(p.producciones.get(0).toString(),""));

                            pila.push(p.producciones.get(0)); // id | numero | simbolo
                            break;
                        case "er":
                            // id -> ERD;
//                            ntemp.agregarHijo(new Nodo(p.producciones.get(0).toString(), ""));
                            ntemp.agregarHijo(new Nodo("erd", ""));

                            pila.push(p.producciones.get(3)); // ;
                            pila.push(ntemp.getHijos().get(0)); // nodo erd
                            pila.push(p.producciones.get(2)); //erd 
                            pila.push(p.producciones.get(1)); // ->
                            pila.push(p.producciones.get(0)); // id
                            break;
                        case "erd":
                            // TE ERDR
                            ntemp.agregarHijo(new Nodo("te", ""));
                            ntemp.agregarHijo(new Nodo("erdr", ""));

                            pila.push(ntemp.getHijos().get(1)); // nodo erdr
                            pila.push(p.producciones.get(1)); // erdr
                            pila.push(ntemp.getHijos().get(0)); //  nodo te
                            pila.push(p.producciones.get(0)); // te
                            break;
                        case "erdr":
                            // ERD | 3 
                            if (p.getProducciones().size() == 0) {
                                continue;
                            }
                            ntemp.agregarHijo(new Nodo("erd", ""));

                            pila.push(ntemp.getHijos().get(0)); // nodo erd
                            pila.push(p.producciones.get(0)); // erd
                            break;
                        case "te":
                            // id | numero | simbolo | cadena
//                            ntemp.agregarHijo(new Nodo(p.producciones.get(0).toString(), ""));

                            pila.push(p.producciones.get(0));
                            break;
                        case "lex":
                            // id : cadena; LEXR
//                            ntemp.agregarHijo(new Nodo(p.producciones.get(0).toString(), ""));
                            ntemp.agregarHijo(new Nodo("lexr", ""));

                            pila.push(ntemp.getHijos().get(0)); // nodo lexr
                            pila.push(p.producciones.get(4)); // lexr
                            pila.push(p.producciones.get(3)); // ;
                            pila.push(p.producciones.get(2)); // cadena
                            pila.push(p.producciones.get(1)); // :
                            pila.push(p.producciones.get(0)); // id
                            break;
                        case "lexr":
                            // LEX | 3
                            if (p.getProducciones().size() == 0) {
                                continue;
                            }

                            ntemp.agregarHijo(new Nodo("lex", ""));

                            pila.push(ntemp.getHijos().get(0)); // nodo lex
                            pila.push(p.producciones.get(0)); // lex
                            break;
                    }
                } else {
                    System.out.println("Error");
                    System.out.println("Se recibe: " + String.valueOf(token));
                    System.out.println("Y se esperaba: " + temp.toString());
                    return; //No nos recuperamos de ningun error.
                }

            }
        } while (!temp.equals("0")); //Esto podria dar error si la entrada contiene un simbolo $

        System.out.println("Se ha terminado de iterar la entrada");

    }

    private int next_token() {
        ++ctoken;
        int retorno = -1;
        if (ctoken <= entrada.size() - 1) {
            retorno = entrada.get(ctoken).getToken();
            while (retorno == 5 || retorno == 8) {
                retorno = entrada.get(++ctoken).getToken();
            }
        }
        return retorno;
    }

    private boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }

        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private int getSymbolToken(String lexema) {
        char a[] = lexema.toCharArray();
        return a[0];
    }

}
