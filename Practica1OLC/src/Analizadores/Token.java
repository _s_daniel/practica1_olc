/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analizadores;

/**
 *
 * @author kc4he
 */
public class Token {
    private final String lexema;
    private final int columna;
    private final int linea;
    private final int token;
    
    public Token(String lexema, int columna, int linea, int token)
    {
        this.lexema = lexema;
        this.columna = columna;
        this.linea = linea;
        this.token = token;
    }

    public String getLexema() {
        return lexema;
    }

    public int getColumna() {
        return columna;
    }

    public int getLinea() {
        return linea;
    }

    public int getToken() {
        return token;
    }
    
    
    
}
