/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analizadores;

/**
 *
 * @author kc4he
 */
public class Error {
    private final String lexema;
    private final int  columna;
    private final int linea;
    private final int token;
    private final String descripcion;

    public Error(String lexema, int columna, int linea, int token, String descripcion) {
        this.lexema = lexema;
        this.columna = columna;
        this.linea = linea;
        this.token = token;
        this.descripcion = descripcion;
    }

    public Error(char c, int columna, int linea, String descripcion) {
        this.lexema = String.valueOf(c);
        this.columna = columna;
        this.linea = linea;
        this.descripcion = descripcion;
        this.token = -1;
    }

    public int getColumna() {
        return columna;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getLexema() {
        return lexema;
    }

    public int getLinea() {
        return linea;
    }

    public int getToken() {
        return token;
    }
    
}
