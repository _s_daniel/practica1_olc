/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analizadores;

import com.google.gson.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import Tabla_Analisis_Sintactico.MatrizOrtogonal;

/**
 *
 * @author da9ni5el
 */
public class TablaAS {

    protected final MatrizOrtogonal tabla_as = new MatrizOrtogonal();

    public TablaAS() {
        leerArchivo();
    }

    private void leerArchivo() {
        File jsonFile = new File("tabla_analisis_sintactico.json");
        if (jsonFile.exists()) {
            try {
                FileReader fr = new FileReader(jsonFile);
                Gson gson = new Gson();
                JsonArray ja = gson.fromJson(fr, JsonArray.class);

                cargarTabla(ja);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TablaAS.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            System.out.println("tablaAs no existe");
        }
    }

    private void cargarTabla(JsonArray ja) {
        for (JsonElement je : ja) {

            JsonObject jo = je.getAsJsonObject();

            //Variables que se extraeran
            String yname = jo.get("yname").getAsString();
            int xtoken = jo.get("xtoken").getAsInt();
            int y = jo.get("y").getAsInt();
            int x = jo.get("x").getAsInt();
            JsonArray producciones = jo.get("produccion").getAsJsonArray();
            
            
            Produccion temp = null;

            if (producciones.isJsonNull()) {
                temp = new Produccion(yname, xtoken, y, x, null);
            } else {
                temp = new Produccion(yname, xtoken, y, x, llenarProducciones(producciones));
            }

            tabla_as.insertar(temp, x, y);
        } 
    }

    private LinkedList<Object> llenarProducciones(JsonArray producciones) {
        LinkedList<Object> p = new LinkedList<>();
        for (JsonElement je : producciones) {
            p.add(je.getAsString());
        }
        return p;
    }

    public void mostrarTablaConsola() {

//        for (int i = 0; i < 16; i++) {
//            for (int j = 0; j < 14; j++) {
//                if (tabla_as[i][j] == null) {
//                    continue;
//                }
//                System.out.println("No terminal: " + tabla_as[i][j].yname);
//                System.out.println("Token: " + tabla_as[i][j].xtoken);
//                System.out.println("pos x: " + j + " pos y: " + i);
//                String produccion = "Produccion: ";
//                if(!tabla_as[i][j].producciones.isEmpty()) {
//                    for (int k = 0; k <= tabla_as[i][j].producciones.size() - 1; k++) {
//                        produccion += tabla_as[i][j].producciones.get(k) + " ";
//                    }
//                    System.out.println(produccion);
//                }
//            }
//        }
    }
}
