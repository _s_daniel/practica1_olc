/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analizadores;

import java.util.LinkedList;
/**
 *
 * @author kc4he
 */
public class Scanner {
    
    private LinkedList<Token> tokens;
    private LinkedList<Error> errores;

    public Scanner(LinkedList<Token> tokens, LinkedList<Error> errores) {
        this.tokens = tokens;
        this.errores = errores;
    }
    
    public void scanner(String entrada) {
        char[] c_entrada = entrada.toCharArray();
        int estado = 0;
        int columna = 1;
        int linea = 1;
        boolean appear_flag;
        StringBuilder temp_lexema = new StringBuilder("");
        for(int i = 0; i < c_entrada.length; i++) {
            switch (estado) {
                case 0:
                    temp_lexema.delete(0, temp_lexema.length());
                    appear_flag = false;
                if(isLetter(c_entrada[i])){ //id / | reservadas
                    ++columna;
                    temp_lexema.append(c_entrada[i]);
                    estado = 1;
                    appear_flag = true;
                } else if(isDigit(c_entrada[i])) { // numeros
                    ++columna;
                    temp_lexema.append(c_entrada[i]);
                    estado = 2;
                    appear_flag = true;
                } else if( c_entrada[i] == 34){ // "
//                    ++columna;
                    temp_lexema.append(c_entrada[i]);
                    estado = 3;
                } else if( c_entrada[i] == 60) { // <
//                    ++columna;
                    temp_lexema.append(c_entrada[i]);
                    estado = 4;
                } else if( c_entrada[i] == 47) { // /
//                  ++columna;
                  temp_lexema.append(c_entrada[i]);
                  estado = 5;
                } else if(c_entrada[i] == 45){ // -
//                    ++columna;
                    temp_lexema.append(c_entrada[i]);
                    estado = 6;
                } else if(c_entrada[i] == 126) {
                  ++columna;
                  temp_lexema.append(c_entrada[i]);
                  estado = 12;
                } else if(isSymbol(c_entrada[i])){ //simbolos
                    if(appear_flag)
                        continue;
                    ++columna;
                    temp_lexema.append(c_entrada[i]);
                    estado = 12;
                } else if(c_entrada[i] >= 0 &&  c_entrada[i] <= 32) { //Se ignoran
                    if(c_entrada[i] == 10) { //esto es redundante
                        ++linea;
                        columna = 1;
                        continue;
                    }
                    ++columna;
                }
                else {
                    //error
                    errores.add(new Error(c_entrada[i], columna, linea, "Caracter desconocido. No forma parte del lenguaje"));
                }
                break;
                case 1:
                    if(isLetterOrDigit(c_entrada[i]) || c_entrada[i] == '_') {
                        ++columna;
                        temp_lexema.append(c_entrada[i]);
                        estado = 1;
                    }
                    else {
                        //Primero compruebo si es palabra reservada
                        estado = 0;
                        i--;
                        if(temp_lexema.toString().toLowerCase().equals("conj")) {
                            tokens.add(new Token(temp_lexema.toString().toLowerCase(), columna, linea, 1)); // CONJ
                            continue;
                        }
                        tokens.add(new Token(temp_lexema.toString().toLowerCase(), columna, linea, 2)); //id
                    }
                    break;
                case 2:
                    if(isDigit(c_entrada[i])) {
                        ++columna;
                        temp_lexema.append(c_entrada[i]);
                        estado = 2;
                    } else {
                        estado = 0;
                        i--;
                        tokens.add(new Token(temp_lexema.toString(), columna, linea, 3)); //numero
                    }
                    break;
                case 3:
                    if(c_entrada[i] != 10) {
                        ++columna;
                        temp_lexema.append(c_entrada[i]);
                        if(c_entrada[i] == 34) // "
                            estado = 13;
                    } else {
                        estado = 0;
                        ++linea;
                        columna = 1;
                        errores.add(new Error(temp_lexema.toString(), columna, linea, -1, "Patron no reconocido"));
                    }
                    break;
                case 4:
                    if(c_entrada[i] == 33){ // !
                        ++columna;
                        temp_lexema.append(c_entrada[i]);
                        estado = 9;
                    } else {
                        errores.add(new Error(temp_lexema.toString(), columna, linea, -1, "Patron no reconocido"));
                        estado = 0;
                    }
                    break;
                case 5:
                    if(c_entrada[i] == 47) { // /
                        ++columna;
                        temp_lexema.append(c_entrada[i]);
                        estado = 10;
                    } else {
                       errores.add(new Error(temp_lexema.toString(), columna, linea, -1, "Patron no reconocido"));
                       estado = 0; 
                    }
                    break;
                case 6:
                    if(c_entrada[i] == 62) {
                        ++columna;
                        temp_lexema.append(c_entrada[i]);
                        estado = 14;
                    }
                    else {
                        errores.add(new Error(temp_lexema.toString(), columna, linea, -1, "Patron no reconocido"));
                        estado = 0;
                    }
                    break;
                case 9:
                    if(c_entrada[i] == 33) {
                        estado = 11;
                    }
                    
                    if(c_entrada[i] == 10) {
                        ++linea;
                        columna = 1;
                    }
                    
                    temp_lexema.append(c_entrada[i]);
                    break;
                case 10:
                    if(c_entrada[i] != 10) {
                        ++columna;
                        temp_lexema.append(c_entrada[i]);
                    } else {
                        estado = 0;
                        ++linea;
                        columna = 1;
                        tokens.add(new Token(temp_lexema.toString(), columna, linea, 5));
                    }
                    break; 
                case 11:
                    if(c_entrada[i] == 62) {
                        estado = 15;
                    } else {
                        estado = 9;
                    }
                    
                    if(c_entrada[i] == 10) {
                        ++linea;
                        columna = 1;
                    }
                    
                    temp_lexema.append(c_entrada[i]);
                    break;
                case 12:
                    estado = 0;
                    i--;
                    
                    if( i+1 <= c_entrada.length) {
                        if(c_entrada[i] == 32 && c_entrada[i+1] != 126){//~
                            continue;
                        }
                    }
                    
                    tokens.add(new Token(temp_lexema.toString(), columna, linea, c_entrada[i]));
                    break;
                case 13: // cadena
                    estado = 0;
                    i--;
                    tokens.add(new Token(temp_lexema.toString(), columna, linea, 6));//cadena
                    break;
                case 14:
                    estado = 0;
                    i--;
                    tokens.add(new Token(temp_lexema.toString(), columna, linea, 7)); // ->
                    break;
                case 15:
                    estado = 0;
                    i--;
                    tokens.add(new Token(temp_lexema.toString(), columna, linea, 8));
                    break;
            }
        }   
    }
    
    
    
    private boolean isLetter(int c) {
        return (c >= 65 && c <= 90) ||  c>=97 && c<=122;
    }
    
    private boolean isDigit(int c) {
        return c >= 48 && c <= 57;
    }
    
    private boolean isLetterOrDigit(int c) {
        return isLetter(c) || isDigit(c);
    }
    
    //Se debe quitar las letras de aqui
    private boolean isSymbol(int c) {
        return c >= 32 && c <= 125; //Podria haber problema al agregar el ~ entre los simbolos
    }

    public LinkedList<Error> getErrores() {
        return errores;
    }

    public LinkedList<Token> getTokens() {
        return tokens;
    }

    public void setErrores(LinkedList<Error> errores) {
        this.errores = errores;
    }

    public void setTokens(LinkedList<Token> tokens) {
        this.tokens = tokens;
    }
    
    
}
