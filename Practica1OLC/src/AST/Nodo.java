/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AST;

import java.util.LinkedList;

/**
 *
 * @author da9ni5el
 */
public class Nodo {
    private String etiqueta;
    private String nombre;
    private Object valor;
    private int tipo;
    private LinkedList<Nodo> hijos;

    public Nodo(String etiqueta, Object valor) {
        this.etiqueta = etiqueta;
        this.valor = valor;
        this.hijos = new LinkedList<>();
        this.nombre = null;
    }
    
    public Nodo(String etiqueta, Object valor, int tipo) {
        this.etiqueta = etiqueta;
        this.valor = valor;
        this.tipo = tipo;
        this.nombre = null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public Nodo() {
        hijos = new LinkedList<>();
        this.tipo = -1;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    public void agregarHijo(Nodo hijo) {
        this.hijos.add(hijo);
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public LinkedList<Nodo> getHijos() {
        return hijos;
    }

    public void setHijos(LinkedList<Nodo> hijos) {
        this.hijos = hijos;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }
    
    
    public String getTexto() {
        StringBuffer sb = new StringBuffer();
        String valor = "";
        
        if(this.getValor() != null) {
            valor = this.getValor().toString();
            if(this.getValor().toString().contains("\"")) {
                valor = this.getValor().toString();
                valor = valor.replace("\"", "");
            }
        }
        
        sb.append("Etiqueta: ").append(this.getEtiqueta());
        
        if(nombre != null) {
            if(!nombre.equals("")) 
                sb.append("\\nNombre: ").append(nombre);
        }
        
        if(valor != null) {
            if(!valor.equals(""))
                sb.append("\\nValor: ").append(valor);
        }
        
        if(tipo != 0) {
            sb.append("\\nTipo: ").append(tipo);
        }
        
        return sb.toString();
    }
}
