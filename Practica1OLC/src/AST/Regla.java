/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AST;

import java.util.LinkedList;

/**
 *
 * @author da9ni5el
 */
public class Regla {

    private int tipo; // 0->rango 1->list, 2->er
    private LinkedList<Integer> rango;
    private LinkedList<Object> er; //aqui pienso guardar la expresion regular
    private String nombre;
    private String valor;

    public Regla() {
        this.tipo = -1;
        this.nombre = null;
        this.valor = null;
        rango = null;
        er = null;
    }

    public Regla(int tipo, LinkedList<Integer> rango, LinkedList<Object> er, String nombre, String valor) {
        this.tipo = tipo;
        this.rango = rango;
        this.er = er;
        this.nombre = nombre;
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LinkedList<Integer> getRango() {
        return rango;
    }

    public void setRango(LinkedList<Integer> rango) {
        this.rango = rango;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public LinkedList<Object> getEr() {
        return er;
    }

    public void setEr(LinkedList<Object> er) {
        this.er = er;
    }
    
    
    public boolean isRangoNull() {
        if(rango == null) {
            return true;
        }
        return false;
    }
    
    public boolean isErNull() {
        if( er == null) {
            return true;
        }
        return false;
    }
    
    public void escribir() {
        StringBuffer sb = new StringBuffer();
        
        if(this.nombre != null) {
            sb.append("Nombre: ").append(this.nombre).append(" ");
        }
        
        if(this.valor != null) {
            sb.append("\nValor: ").append(this.valor).append(" ");
        }
        
        if(!this.isErNull()) {
            sb.append(escribirLista(er));
        }
        
        if(!this.isRangoNull()) {
            sb.append(escribirLista(rango));
        }
        
        System.out.println(sb.toString());
    }
    
    private String escribirLista(LinkedList l) {
        
        StringBuffer sb = new StringBuffer();
        
        for(Object a: l) {
            sb.append(a.toString()).append(" ");
        }
        
        return sb.toString();
    }

}
