/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AST;

import java.util.HashMap;

/**
 *
 * @author da9ni5el
 */
public class Entorno {
    
    public HashMap<String, Simbolo> tabla;

    public Entorno() {
        this.tabla = new HashMap<>();
    }
    
    public void insertar(String nombre, Simbolo sim) {
        if(tabla.containsKey(nombre)) {
            System.out.println("El valor ya existe");
            return;
        }
        
        tabla.put(nombre, sim);
    }
    
    
    public Simbolo buscar(String nombre) {
        if(tabla.containsKey(nombre)) {
            return (Simbolo)tabla.get(nombre);
        }
        return null;
    }
    
    public boolean sustituir(String key, Simbolo nuevo) {
        if(tabla.replace(key, nuevo) != null) {
            return true;
        }
        return false;
    }
}
