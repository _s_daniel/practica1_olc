/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AST;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author da9ni5el
 */
public class Ejecutor {

    private LinkedList<Regla> Lexemas;
    private LinkedList<Regla> er;

    public Ejecutor() {
        Lexemas = new LinkedList<>();
        er = new LinkedList<>();
    }

    public void ejecutar(Nodo raiz, Entorno e) {
        recorrer(raiz, e);
    }

    private void recorrer(Nodo raiz, Entorno e) {
        if (raiz == null) {
            return;
        }

        switch (raiz.getEtiqueta()) {
            case "inicio":
                recorrer(raiz.getHijos().get(0), e);
                break;
            case "cuerpo":
                recorrer(raiz.getHijos().get(0), e);
                recorrer(raiz.getHijos().get(1), e);
                break;
            case "dec":
                recorrer(raiz.getHijos().get(0), e);
                recorrer(raiz.getHijos().get(1), e);
                break;
            case "decr":
                if (raiz.getHijos().size() > 0) {
                    recorrer(raiz.getHijos().get(0), e);
                }
                break;
            case "conj":
                //llamar a una subrutina
                conj(raiz, e);
                break;
            case "er":
                //Llamada a una subrutina
                er(raiz, e);
                break;
            case "lex":
                //llamada a una subrutina
                lex(raiz, e);
                break;
            default:
                //aqui lo mas inteligente seria recorrer la lista de hijos de raiz y ejecutar recursivamente.
                System.out.println("Nodo actual: " + raiz.getEtiqueta() + " " + raiz.getValor());
                break;
        }
    }

    private void conj(Nodo raiz, Entorno e) {
        System.out.println("Co-rutina conj");
        if (raiz == null) {
            System.out.println("Conj: Raiz nula");
            return;
        }

        Regla r = new Regla();
        String tnombre = raiz.getValor().toString();
        r.setNombre(tnombre);
        extraerConj(raiz.getHijos().get(0), r); //aqui siempre le envio conjc

        //en este punto ya llene r
        Simbolo ts = new Simbolo(Simbolo.EnumTipo.conj, r);
        e.insertar(tnombre, ts); //No podrian existir dos rangos iguales

        System.out.println("Se extrajo la informacion de Conj");
    }

    private void extraerConj(Nodo raiz, Regla r) {
        if (raiz == null) {
            return;
        }

        switch (raiz.getEtiqueta()) {
            case "conjc":
                extraerConj(raiz.getHijos().get(0), r);
                extraerConj(raiz.getHijos().get(1), r);
                break;
            case "t":
                if (r.isRangoNull()) {
                    r.setRango(new LinkedList<>());
                }

                r.getRango().add(getAscii(raiz.getValor().toString()));
                break;
            case "op":
                if (raiz.getHijos().get(0).getEtiqueta().equals("t")) {
                    // A~A
                    r.setTipo(0);
                } else {
                    //Lista
                    r.setTipo(1);
                }

                extraerConj(raiz.getHijos().get(0), r);
                break;
            case "list":
                extraerConj(raiz.getHijos().get(0), r);
                extraerConj(raiz.getHijos().get(1), r);
                break;
            case "listr":
                if (raiz.getHijos().size() > 0) {
                    extraerConj(raiz.getHijos().get(0), r);
                }
                break;
        }

    }

    private void er(Nodo raiz, Entorno e) {
        System.out.println("Co-rutina er");

        if (raiz == null) {
            System.out.println("Er: Raiz nula");
            return;
        }

        Regla r = new Regla();
        String tnombre = raiz.getValor().toString();
        r.setNombre(tnombre);
        r.setTipo(2); // tipo de er

        extraerEr(raiz.getHijos().get(0), r, er,e);

        //Agregar a la tabla de simbolos
        Simbolo s = new Simbolo(Simbolo.EnumTipo.er, r);
        e.insertar(tnombre, s);
        er.add(r);

        System.out.println("Se ha terminado de capturar la informaicon de er. Tam: " + er.size());
    }

    private void extraerEr(Nodo raiz, Regla r, LinkedList er, Entorno e) {
        if (raiz == null) {
            return;
        }

        switch (raiz.getEtiqueta()) {
            case "erd":
                extraerEr(raiz.getHijos().get(0), r, er,e);
                extraerEr(raiz.getHijos().get(1), r, er,e);
                break;
            case "te":
                if (r.isErNull()) {
                    r.setEr(new LinkedList<>());
                }
                //esto podria generar errores. Al no reconocer las llaves como parte de las expresiones regulares
                if (raiz.getValor().equals("{") || raiz.getValor().equals("}")) {
                    break;
                }
                
                if(raiz.getTipo() == 2) { //esto es un id
                    if(e.buscar(raiz.getValor().toString()) == null) {
                        System.out.println("Error, el conjunto no se ha definido con anterioridad");
                    }
                }

//                r.getEr().add(raiz.getValor());
                r.getEr().add(new Simbolo(escogerTipo(raiz.getTipo()), raiz.getValor()));
                break;
            case "erdr":
                if (raiz.getHijos().size() > 0) {
                    extraerEr(raiz.getHijos().get(0), r, er,e);
                }
                break;
        }

    }

    private void lex(Nodo raiz, Entorno e) {
        System.out.println("Co-rutina lex");
        extraerLex(raiz, Lexemas, e);
        System.out.println("Se han terminado de capturar los valores de LEX. Tam: " + Lexemas.size());
    }

    private void extraerLex(Nodo raiz, LinkedList lex, Entorno e) {
        if (raiz == null) {
            return;
        }

        if (raiz.getEtiqueta().equals("lex")) {
            //Aqui se crea la regla
            String tnombre = raiz.getNombre();
            String tvalor = raiz.getValor().toString();
            int tipo = raiz.getTipo();

            Regla r = new Regla(tipo, null, null, tnombre, tvalor);
            lex.add(r);
        }

        if (raiz.getHijos().size() > 0) {
            extraerLex(raiz.getHijos().get(0), lex, e);
        }
    }

    public LinkedList<Regla> getLexemas() {
        return Lexemas;
    }
    
    public LinkedList<Regla> getEr() {
        return er;
    }

    public int getAscii(Object o) {
        int retorno = -1;
        if (o instanceof String) {
            char temp[] = o.toString().toCharArray();
            retorno = temp[0];
        } else {
            char temp = (char) o;
            retorno = temp;
        }
        System.out.println(retorno);
        return retorno;
    }
    
    public Simbolo.EnumTipo escogerTipo(int tipo) {
        Simbolo.EnumTipo temp = null;
        switch(tipo) {
            case 2:
                temp = Simbolo.EnumTipo.id;
                break;
            case 3:
                temp = Simbolo.EnumTipo.numero;
                break;
            case 6:
                temp = Simbolo.EnumTipo.cadena;
                break;
            default:
                if(tipo >= 32 && tipo <= 125) {
                    temp = Simbolo.EnumTipo.simbolo;
                }
                break;
        }
        
        return temp;
    }

    //Borrar mas adelante
    public void graficarABB(String nombre, Nodo raiz) {
        if (raiz == null) {
            System.out.println("Arbol vacio");
            return;
        }
        try {
            try (BufferedWriter dotcode = new BufferedWriter(new FileWriter(new File(nombre + ".dot")))) {
                dotcode.write("digraph G{\ngraph[overlap=true, fontsize = 0.5];\nnode[shape=ellipse, fontname=Helvetica, fixedsize=true, width=3.5, height=0.9];\n");
                dotcode.write("edge[color = black];\n");
                dotcode.write(generarDot(raiz));
                dotcode.write("}");
                dotcode.close();
            }

            Runtime rt = Runtime.getRuntime();
            rt.exec("dot -Tpng " + nombre + ".dot -o src/img/" + nombre + ".png");
            rt.exec("display src/img/" + nombre + ".png");
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private String generarDot(Nodo raiz) {
        StringBuffer sb = new StringBuffer();

        if (raiz == null) {
            return "";
        }

        sb.append("nodo").append(raiz.hashCode()).append("[label=\"");
        sb.append(raiz.getTexto());
        sb.append("\"];\n");

        for (int i = 0; i < raiz.getHijos().size(); i++) {
            Nodo temporal = raiz.getHijos().get(i);
            if (temporal != null) {
                sb.append("nodo").append(temporal.hashCode()).append("[label=\"");
                sb.append(temporal.getTexto());
                sb.append("\"];\n");

                //Ahora hago el enlace correspondiente.
                sb.append("nodo").append(raiz.hashCode());
                sb.append("->");
                sb.append("nodo").append(temporal.hashCode());
                sb.append("[label=\"Hijo").append(i).append("\"];\n");

                sb.append(generarDot(temporal));
            }
        }

        return sb.toString();
    }

}
