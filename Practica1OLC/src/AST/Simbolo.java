/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AST;

/**
 *
 * @author da9ni5el
 */
public class Simbolo {
    public EnumTipo Enum;
    public Object valor;

    public Simbolo() {
    }

    public Simbolo(EnumTipo Enum, Object valor) {
        this.Enum = Enum;
        this.valor = valor;
    }
    
    public enum EnumTipo {
        lex, er, conj, id, numero, cadena, simbolo, dob
    }
      
}
