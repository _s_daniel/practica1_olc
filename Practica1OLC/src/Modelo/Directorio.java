/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.File;

/**
 *
 * @author da9ni5el
 */
public class Directorio {
    private File file;
    private int posicion; //lo usare para saber en que tab lo meti

    public Directorio(File file) {
        this.file = file;
    }
    
    public Directorio(File file, int pos) {
        this.file = file;
        this.posicion = pos;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }
    
   
    @Override
    public String toString() {
        if(!"".equals(file.getName())) {
            return file.getName();
        } else {
            return file.getPath();
        }
    }
}
