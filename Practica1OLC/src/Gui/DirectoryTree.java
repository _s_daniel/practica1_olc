/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui;

import java.io.File;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import Modelo.Directorio;
import javax.swing.event.TreeSelectionListener;

/**
 *
 * @author da9ni5el
 */
public class DirectoryTree implements TreeSelectionListener{
    private DefaultMutableTreeNode raiz;
    private DefaultTreeModel modelo;
    private JTree arbol;
    private File currentFile;

    public DirectoryTree() {
    }

    public void insertarHijo(File root) {
        insertarNodo(root, raiz);
    }

    private void insertarNodo(File fileRoot, DefaultMutableTreeNode nodo) {
        File files[] = fileRoot.listFiles();
        if (files == null) {
            return;
        }

        for (File file : files) {
            DefaultMutableTreeNode nuevo = new DefaultMutableTreeNode(new Directorio(file));
            nodo.add(nuevo);
            if (file.isDirectory()) {
                insertarNodo(file, nuevo);
            }
        }
    }

    protected void crearArbol(File root) {
        raiz = new DefaultMutableTreeNode(new Directorio(root));
        modelo = new DefaultTreeModel(raiz);

        arbol = new JTree(modelo);

        arbol.getSelectionModel().addTreeSelectionListener(this);
    }

    public File getCurrentFile() {
        return currentFile;
    }
    
    public JTree getDirectorio() {
        return this.arbol;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode nodoSeleccionado;
        nodoSeleccionado = (DefaultMutableTreeNode) arbol.getLastSelectedPathComponent();
        currentFile = ((Directorio)nodoSeleccionado.getUserObject()).getFile();
    }
}
