/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui;

import AST.Ejecutor;
import AST.Entorno;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;

import javax.swing.JTextArea;
import javax.swing.ScrollPaneLayout;
import javax.swing.filechooser.FileNameExtensionFilter;



/*Importando mis paquetes*/
import Analizadores.Scanner;
import Analizadores.Token;
import Analizadores.Error;
import AST.Nodo;
import AST.Regla;
import Analizadores.Parser;
import Implementacion.Arbol;
/**
 *
 * @author da9ni5el
 */
public class Principal extends javax.swing.JFrame {

    private String nombreArchivo;
    private LinkedList<String> archivosAbiertos;
    private LinkedList<Token> ltokens;
    private LinkedList<Error> lerrores;
    private Ejecutor ejecutor;
    private Nodo raiz;
    private Entorno global;
    private LinkedList<Arbol> arboles;
    
    private Scanner scanner;
    private Parser parser;

    String prompt = ">> ";

    public Principal() {
        initComponents();
        generarAFD.setEnabled(false);
        evaluarLexemas.setEnabled(false);
        jMenuItem2.setEnabled(false);
        
        
        nombreArchivo = "";
        archivosAbiertos = new LinkedList<>();
    }

    /*No tocar*/
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtConsola = new javax.swing.JTextArea();
        contenedorP = new javax.swing.JPanel();
        tabPane = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        mArchivo = new javax.swing.JMenu();
        mCrearNuevo = new javax.swing.JMenuItem();
        mAbrirArchivo = new javax.swing.JMenuItem();
        mGuardar = new javax.swing.JMenuItem();
        mGuardarComo = new javax.swing.JMenuItem();
        mSalir = new javax.swing.JMenuItem();
        mAnalizar = new javax.swing.JMenu();
        mEjecutar = new javax.swing.JMenuItem();
        generarAFD = new javax.swing.JMenuItem();
        evaluarLexemas = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        mAcercaDe = new javax.swing.JMenu();
        mEstudiante = new javax.swing.JMenuItem();
        mPractica = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Practica 1");
        setResizable(false);

        txtConsola.setEditable(false);
        txtConsola.setColumns(20);
        txtConsola.setRows(5);
        jScrollPane1.setViewportView(txtConsola);

        tabPane.setMaximumSize(new java.awt.Dimension(750, 383));

        javax.swing.GroupLayout contenedorPLayout = new javax.swing.GroupLayout(contenedorP);
        contenedorP.setLayout(contenedorPLayout);
        contenedorPLayout.setHorizontalGroup(
            contenedorPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorPLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabPane, javax.swing.GroupLayout.PREFERRED_SIZE, 750, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        contenedorPLayout.setVerticalGroup(
            contenedorPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorPLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabPane, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        mArchivo.setText("Archivo");

        mCrearNuevo.setText("Crear Nuevo");
        mCrearNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCrearNuevoActionPerformed(evt);
            }
        });
        mArchivo.add(mCrearNuevo);

        mAbrirArchivo.setText("Abrir Archivo");
        mAbrirArchivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mAbrirArchivoActionPerformed(evt);
            }
        });
        mArchivo.add(mAbrirArchivo);

        mGuardar.setText("Guardar");
        mGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mGuardarActionPerformed(evt);
            }
        });
        mArchivo.add(mGuardar);

        mGuardarComo.setText("Guardar Como");
        mGuardarComo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mGuardarComoActionPerformed(evt);
            }
        });
        mArchivo.add(mGuardarComo);

        mSalir.setText("Salir");
        mSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mSalirActionPerformed(evt);
            }
        });
        mArchivo.add(mSalir);

        jMenuBar1.add(mArchivo);

        mAnalizar.setText("Herramientas");

        mEjecutar.setText("Ejecutar Analisis");
        mEjecutar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mEjecutarActionPerformed(evt);
            }
        });
        mAnalizar.add(mEjecutar);

        generarAFD.setText("Generar AFD");
        generarAFD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generarAFDActionPerformed(evt);
            }
        });
        mAnalizar.add(generarAFD);

        evaluarLexemas.setText("Evaluar Lexemas");
        evaluarLexemas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                evaluarLexemasActionPerformed(evt);
            }
        });
        mAnalizar.add(evaluarLexemas);

        jMenuItem2.setText("Generar AST");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        mAnalizar.add(jMenuItem2);

        jMenuBar1.add(mAnalizar);

        mAcercaDe.setText("Acerca De");

        mEstudiante.setText("Datos Estudiante");
        mAcercaDe.add(mEstudiante);

        mPractica.setText("Acerca de la Practica");
        mAcercaDe.add(mPractica);

        jMenuBar1.add(mAcercaDe);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(contenedorP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 3, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(contenedorP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //750,383
    private void crearTabPane(String nombre) {
        //740,375
        javax.swing.JTextArea editor = new javax.swing.JTextArea(740, 375);
        editor.setVisible(rootPaneCheckingEnabled);
        editor.setLayout(new GridLayout());

        javax.swing.JScrollPane scroll = new JScrollPane();
        scroll.setSize(740, 375);
        scroll.setLayout(new ScrollPaneLayout());
        scroll.add(editor);
        scroll.setViewportView(editor);

        tabPane.addTab(nombre, scroll);
    }

    
    
    
    private JTextArea recuperarEditor(int i) {
        JScrollPane jscroll = (JScrollPane) tabPane.getComponent(i);
        tabPane.setSelectedComponent(tabPane.getComponent(i));
        JTextArea editor = (JTextArea) jscroll.getViewport().getComponent(0);

        return editor;
    }

    private String leerArchivo() {
        StringBuffer buffer = new StringBuffer();
        String aux = "";
        JFileChooser jfc = new JFileChooser();
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter(".Er Files", "er");
        jfc.setFileFilter(filtroImagen);
        jfc.showOpenDialog(this);

        File cargar = jfc.getSelectedFile();

        if (cargar != null) {
            try {
                nombreArchivo = cargar.getName();
                archivosAbiertos.add(cargar.getAbsolutePath());
                FileReader fr = new FileReader(cargar);
                BufferedReader bfr = new BufferedReader(fr);
                while ((aux = bfr.readLine()) != null) {
                    buffer.append(aux).append("\n");
                }
                bfr.close();

            } catch (IOException e) {
                System.out.println("error");
                buffer = null;
            }
        }
        return buffer.toString();
    }


    private void mAbrirArchivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mAbrirArchivoActionPerformed
        String texto = leerArchivo();
        if (!texto.isEmpty()) {
            crearTabPane(nombreArchivo);
            JTextArea editor = recuperarEditor(tabPane.getComponentCount() - 1);

            //Utilizar selected index para el tabbedpane
            if (editor != null) {
                editor.setText(texto);
            } else {
                System.out.println("Imposible cargar el archivo");
            }
        }

    }//GEN-LAST:event_mAbrirArchivoActionPerformed

    private void mSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_mSalirActionPerformed

    private void mCrearNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mCrearNuevoActionPerformed
        crearTabPane("Nuevo");
        tabPane.setSelectedComponent(tabPane.getComponent(tabPane.getComponentCount() - 1));
    }//GEN-LAST:event_mCrearNuevoActionPerformed

    private void mGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mGuardarActionPerformed
        // TODO add your handling code here:
        int selectedIndex = tabPane.getSelectedIndex();
        if (archivosAbiertos.get(selectedIndex) == null) {
            return;
        }

        JTextArea editor = recuperarEditor(tabPane.getSelectedIndex());
        if (editor != null) {
            try {
                try (BufferedWriter bfw = new BufferedWriter(new FileWriter(new File(archivosAbiertos.get(selectedIndex))))) {
                    bfw.write(editor.getText());
                }
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
    }//GEN-LAST:event_mGuardarActionPerformed

    private void mGuardarComoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mGuardarComoActionPerformed
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogTitle("Selecciona la ruta para guardar el archivo");

        int seleccion = jfc.showSaveDialog(this);

        if (seleccion == JFileChooser.APPROVE_OPTION) {
            File guardar = jfc.getSelectedFile();

            JTextArea editor = recuperarEditor(tabPane.getSelectedIndex());
            try {
                try (BufferedWriter bfw = new BufferedWriter(new FileWriter(guardar))) {
                    bfw.write(editor.getText());
                }

            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
    }//GEN-LAST:event_mGuardarComoActionPerformed

    private void mEjecutarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mEjecutarActionPerformed
        //Ejecutar aqui el archivo de reportes
        JTextArea editor = recuperarEditor(tabPane.getSelectedIndex());
        if (editor.getText() != "") {
            interpretar(editor.getText());
        }
    }//GEN-LAST:event_mEjecutarActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        ejecutor.graficarABB("ast", raiz);
        
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void generarAFDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generarAFDActionPerformed
        //Lleno mi lista de arboles con las Expresiones regulares
        LinkedList<Regla> temp = ejecutor.getEr();
        arboles = new LinkedList<>();
        
        for(Regla r: temp) {
            arboles.add(new Arbol(r.getEr(), r.getNombre()));
            System.out.println(r.getNombre());
        }      
        
        
        //Ahora envio uno por uno a ejecutarse.
        for (Arbol arbole : arboles) {
            arbole.generarGrafo();      
            arbole.graficarArbol();
        }
    }//GEN-LAST:event_generarAFDActionPerformed

    private void evaluarLexemasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_evaluarLexemasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_evaluarLexemasActionPerformed

    private void interpretar(String texto) {
        //Con esto generaria una lista nueva cada vez que analizo
        ltokens = new LinkedList<>();
        lerrores = new LinkedList<>();
        scanner = new Scanner(ltokens, lerrores);
        scanner.scanner(texto);
        
        if(!lerrores.isEmpty()) {
            escribirLog("Errores Lexicos encontrados");
        } else {
            escribirLog("Se ha termiando de ejecutar el analisis sintactico.");
            ltokens.add(new Token("$", -1, -1, 0));
            parser = new Parser(ltokens);
            
            escribirLog("Ejecutando Parser");
            parser.parse();
            
            escribirLog("Parser Ejecutado correctamente");
            raiz = parser.getRaiz();
            
            global = new Entorno();
            ejecutor = new Ejecutor();
            ejecutor.ejecutar(raiz, global);
            
            generarAFD.setEnabled(true);
            evaluarLexemas.setEnabled(true);
            jMenuItem2.setEnabled(true);
            
        }
        
    }

    private void escribirLog(String args) {

        txtConsola.append(prompt + args + "\n");
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel contenedorP;
    private javax.swing.JMenuItem evaluarLexemas;
    private javax.swing.JMenuItem generarAFD;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem mAbrirArchivo;
    private javax.swing.JMenu mAcercaDe;
    private javax.swing.JMenu mAnalizar;
    private javax.swing.JMenu mArchivo;
    private javax.swing.JMenuItem mCrearNuevo;
    private javax.swing.JMenuItem mEjecutar;
    private javax.swing.JMenuItem mEstudiante;
    private javax.swing.JMenuItem mGuardar;
    private javax.swing.JMenuItem mGuardarComo;
    private javax.swing.JMenuItem mPractica;
    private javax.swing.JMenuItem mSalir;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JTextArea txtConsola;
    // End of variables declaration//GEN-END:variables

}
