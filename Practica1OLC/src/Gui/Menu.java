/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author da9ni5el
 */
public class Menu {
    protected JMenu archivo;
    protected JMenu reporte;
    protected JMenuBar bar;

    protected JMenuItem crearProyecto;
    protected JMenuItem crearArchivo;
    protected JMenuItem GuardarArchivo;
    protected JMenuItem GuardarComo;
    protected JMenuItem CerrarArchivo;
    protected JMenuItem CerrarProyecto;
    protected JMenuItem CompilarProyecto;
    
    public Menu() {
//        jmenu = new JMenu("");
    }

    public Menu(JMenuBar bar) {
        
        this.bar = bar;
        
        inicializarBar();
        
    }
    
    
    protected void inicializarBar()
    {
        //Probar si esto funciona bien 
        archivo = new JMenu("Archivo");
        
        crearEventos(archivo);
        
        bar.add(archivo);
        
    }
    
    
    
    
    private void crearEventos(JMenu archivo)
    {
        crearProyecto = new JMenuItem("Crear Proyecto");
        crearArchivo = new JMenuItem("Crear Archivo");
        GuardarArchivo = new JMenuItem("Guardar Archivo");
        GuardarComo = new JMenuItem("Guardar Como");
        CerrarArchivo = new JMenuItem("Cerrar Archivo");
        CerrarProyecto = new JMenuItem("Cerrar Proyecto");
        CompilarProyecto = new JMenuItem("Compilar Proyecto");
        
        crearProyecto.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                System.out.println("Crear Proyecto button");
            }
        });
        
        crearArchivo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        
        
        
        archivo.add(crearProyecto);
        archivo.add(crearArchivo);
        archivo.add(GuardarArchivo);
        archivo.add(GuardarComo);
        archivo.add(CerrarArchivo);
        archivo.add(CerrarProyecto);
        archivo.add(CompilarProyecto);        
    }
}
