/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabla_Analisis_Sintactico;


import Analizadores.Produccion;
import java.io.IOException;
import java.io.PrintWriter;

//[dir=both]
/**
 *
 * @author da9ni5el
 */
public class MatrizOrtogonal {

    protected ListaCabecera filas;
    protected ListaCabecera columnas;

    public MatrizOrtogonal() {
        this.filas = new ListaCabecera();
        this.columnas = new ListaCabecera();
    }
    

    public void insertar(Produccion valor, int x, int y)//x columna, y fila
    {
        NodoOrtogonal nuevo = new NodoOrtogonal(valor, x, y);

        NodoCabecera fila = filas.getNodoCabecera(y);
        insertarFilas(nuevo, fila);

        NodoCabecera columna = columnas.getNodoCabecera(x);
        insertarColumna(nuevo, columna);
    }

    private void insertarFilas(NodoOrtogonal nuevo, NodoCabecera fila) {
        if (fila == null) {
            fila = new NodoCabecera(nuevo.getY(), nuevo.getDato().getYname());
            filas.insertar(fila);
            fila.setAcceso(nuevo);
        } else {
            if (nuevo.getX() < fila.getAcceso().getX()) {
                nuevo.setDcho(fila.getAcceso());
                fila.getAcceso().setIzdo(nuevo);
                fila.setAcceso(nuevo);
            } else {
                NodoOrtogonal pivote = fila.getAcceso();
                while (nuevo.getX() >= pivote.getX()) {
                    if (nuevo.getDato().igualQue(pivote.getDato(), 0)) {
                        pivote.dato = nuevo.dato;
                        return;
                    }

                    if (pivote.getDcho() == null) {
                        pivote.setDcho(nuevo);
                        nuevo.setIzdo(pivote);
                        return;
                    }
                    pivote = pivote.getDcho();
                }
                pivote.getIzdo().setDcho(nuevo);
                nuevo.setIzdo(pivote.getIzdo());
                nuevo.setDcho(pivote);
                pivote.setIzdo(nuevo);
            }
        }
    }

    private void insertarColumna(NodoOrtogonal nuevo, NodoCabecera columna) {
        if (columna == null) {
            columna = new NodoCabecera(nuevo.getX(), nuevo.getDato().getXtoken());
            columnas.insertar(columna);
            columna.setAcceso(nuevo);
        } else {
            if (nuevo.getY() < columna.getAcceso().getY()) {
                nuevo.setAbjo(columna.getAcceso());
                columna.getAcceso().setArrb(nuevo);
                columna.setAcceso(nuevo);
            } else {
                NodoOrtogonal pivote = columna.getAcceso();
                while (nuevo.getY() >= pivote.getY()) {
                    if (nuevo.getDato().igualQue(pivote.getDato(), 0)) {
                        pivote.dato = nuevo.dato;
                        return;
                    }

                    if (pivote.getAbjo() == null) {
                        pivote.setAbjo(nuevo);
                        nuevo.setArrb(pivote);
                        return;
                    }
                    pivote = pivote.getAbjo();
                }
                pivote.getArrb().setAbjo(nuevo);
                nuevo.setArrb(pivote.getArrb());
                nuevo.setAbjo(pivote);
                pivote.setArrb(nuevo);
            }
        }
    }
    
    public Produccion buscar(int x, int y) {
        NodoOrtogonal buscado = buscarNodoOrtogonal(y, x);
        if(buscado != null) {           
            return  buscado.getDato();                        
        }
        return null;
    }

    public Produccion buscar(Object Nt, Object T) {
        NodoOrtogonal buscado = buscarNodoOrtogonal(Nt, T);
        if(buscado != null) {
            return buscado.getDato();
        }
        return null;
    }
    
    protected NodoOrtogonal buscarNodoOrtogonal(int y, int x) {
        NodoCabecera efila = filas.getNodoCabecera(y);
        if (efila == null) {
            return null;
        }
        NodoOrtogonal pivote = efila.getAcceso();
        while (pivote != null) {
            if (pivote.getX() == x) {
                return pivote;
            }
            pivote = pivote.getDcho();
        }
        return null;
    }

    protected NodoOrtogonal buscarNodoOrtogonal(Object Nt, Object T) {
        NodoCabecera efila = filas.buscarPorDesc(Nt);
        if (efila == null) {
            return null;
        }
        NodoOrtogonal pivote = efila.getAcceso();
        while (pivote != null) {
            if (T.equals(pivote.getDato().getXtoken())) {
                return pivote;
            } else {
                if(pivote.getDato().getXtoken() == 200) {
                    if(Integer.valueOf(T.toString()) >= 32 && Integer.valueOf(T.toString()) <= 125) {
                        return pivote;
                    }
                }
            }
            pivote = pivote.getDcho();
        }
        return null;
    }
    
    public void escribirConsola(){ //Recorrido por filas
        NodoCabecera pivote = filas.primero;
        System.out.println("IMPRIMIENDO POR FILAS\n");
        do
        {
            if(pivote != null)
            {
                System.out.println("No terminal: " + pivote.getDesc().toString());
                
                NodoOrtogonal aux = pivote.acceso;
                while(aux != null) {
                    System.out.println("Terminal: " + aux.getDato().getXtoken());
                    
                    String p="Produccion: ";
                    
                    if(aux.getDato().getProducciones() == null)
                        continue;
                    
                    
                    if(!aux.getDato().getProducciones().isEmpty()){
                        for(Object o : aux.getDato().getProducciones()) {
                            p+= o.toString() + " ";
                        }
                    } else {
                       p+="EPSILON";
                    }
                    
                    System.out.println(p);
                    System.out.println("");
                    aux = aux.getDcho();
                }
                System.out.println("");
                pivote = pivote.getSiguiente();
            }
        }while(pivote!= null && pivote!= filas.ultimo.getSiguiente());
        
        System.out.println("\n");
    }    
}
    