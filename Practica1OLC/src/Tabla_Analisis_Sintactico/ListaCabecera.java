/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabla_Analisis_Sintactico;

/**
 *
 * @author da9ni5el
 * @param 
 */
public class ListaCabecera  {
    protected NodoCabecera primero;
    protected NodoCabecera ultimo;
    protected int tam;

    public ListaCabecera() {
        this.primero = null;
        this.ultimo = null;
        this.tam = 0;        
    }
    
    public void insertar(NodoCabecera nuevo)
    {
        this.tam++;
        if(primero == null)
            primero = ultimo = nuevo;
        else
        {
            if(nuevo.getPosicion() < primero.getPosicion())
            {
                nuevo.setSiguiente(primero);
                nuevo.setAnterior(ultimo);
                ultimo.setSiguiente(nuevo);
                primero.setAnterior(nuevo);
                primero = nuevo;
            }
            else
            {
                NodoCabecera pivote = primero;
                while(nuevo.getPosicion() > pivote.getPosicion())
                {
                    if(pivote == ultimo)
                    {
                        ultimo.siguiente = nuevo;
                        nuevo.anterior = ultimo;
                        nuevo.siguiente = primero;
                        primero.anterior = nuevo;
                        ultimo = nuevo;
                        return;
                    }
                    pivote = pivote.getSiguiente();                    
                }
                pivote.getAnterior().setSiguiente(nuevo);
                nuevo.setAnterior(pivote.getAnterior());
                nuevo.setSiguiente(pivote);
                pivote.setAnterior(nuevo);
            }
        }
    }
    
    protected NodoCabecera getNodoCabecera(int posicion)
    {
        if(primero == null) return null;
        NodoCabecera pivote = primero;
        do
        {
            if(pivote != null)
            {
                if(pivote.getPosicion() == posicion)
                    return pivote;
                pivote = pivote.getSiguiente();
            }
        }while(pivote!= null && pivote!= ultimo.getSiguiente());
        return null;
    }        
    
    protected NodoCabecera buscarPorDesc(Object buscado) {
        if(primero == null) return null;
        NodoCabecera pivote = primero;
        do
        {
            if(pivote != null)
            {
                if(pivote.getDesc().equals(buscado))
                    return pivote;
                pivote = pivote.getSiguiente();
            }
        }while(pivote!= null && pivote!= ultimo.getSiguiente());
        return null;
    }
    
    protected void eliminarCabecera(int posicion)
    {
        NodoCabecera auxiliar = getNodoCabecera(posicion);
        if(auxiliar!= null)
        {
            auxiliar.getSiguiente().setAnterior(auxiliar.getAnterior());
            auxiliar.getAnterior().setSiguiente(auxiliar.getSiguiente());
            if(auxiliar == primero)
                primero = auxiliar.getSiguiente();
            if(auxiliar == ultimo)
                ultimo = auxiliar.getAnterior();
            auxiliar = null;
            this.tam--;
            System.out.println("Probando el eliminar");            
        }        
    }
    
    protected void recorrer()
    {
        NodoCabecera pivte = primero;
        do
        {
            if(pivte != null)
            {
                System.out.println("Dato cabecera: " +  pivte.posicion);
                pivte = pivte.siguiente;
            }            
        }while(pivte != null && pivte != primero);
        
    }
}
