/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabla_Analisis_Sintactico;


/**
 *
 * @author da9ni5el
 * 
 */
public class NodoCabecera {
    protected int posicion;
    protected Object desc;
    protected NodoCabecera siguiente;
    protected NodoCabecera anterior;
    protected NodoOrtogonal acceso;

    public NodoCabecera(int posicion) {
        this.posicion = posicion;
        this.siguiente = null;
        this.anterior = null;
        this.acceso = null;
    }
    
    public NodoCabecera(int posicion, Object desc) {
        this.posicion = posicion;
        this.desc = desc;
        this.siguiente = null;
        this.anterior = null;
        this.acceso = null;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public NodoCabecera getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoCabecera siguiente) {
        this.siguiente = siguiente;
    }

    public NodoCabecera getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoCabecera anterior) {
        this.anterior = anterior;
    }

    public NodoOrtogonal getAcceso() {
        return acceso;
    }

    public void setAcceso(NodoOrtogonal acceso) {
        this.acceso = acceso;
    }

    public Object getDesc() {
        return desc;
    }

    public void setDesc(Object desc) {
        this.desc = desc;
    }
    
    
    
}
