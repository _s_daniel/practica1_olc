/*
 * Producciono change this license header, choose License Headers in Project Properties.
 * Producciono change this template file, choose Produccionools | Produccionemplates
 * and open the template in the editor.
 */
package Tabla_Analisis_Sintactico;

import Analizadores.Produccion;

/**
 *
 * @author da9ni5el
 * @param 
 */
public class NodoOrtogonal {
    protected Produccion dato;
    protected int x;
    protected int y;
    protected NodoOrtogonal dcho;
    protected NodoOrtogonal izdo;
    protected NodoOrtogonal abjo;
    protected NodoOrtogonal arrb;

    public NodoOrtogonal(Produccion dato, int x, int y) {
        this.dato = dato;
        this.dcho = null;
        this.izdo = null;
        this.abjo = null;
        this.arrb = null;
        this.x = x;
        this.y = y;
    }
        
    public Produccion getDato() {
        return dato;
    }

    public void setDato(Produccion dato) {
        this.dato = dato;
    }

    public NodoOrtogonal getDcho() {
        return dcho;
    }

    public void setDcho(NodoOrtogonal dcho) {
        this.dcho = dcho;
    }

    public NodoOrtogonal getIzdo() {
        return izdo;
    }

    public void setIzdo(NodoOrtogonal izdo) {
        this.izdo = izdo;
    }

    public NodoOrtogonal getAbjo() {
        return abjo;
    }

    public void setAbjo(NodoOrtogonal abjo) {
        this.abjo = abjo;
    }

    public NodoOrtogonal getArrb() {
        return arrb;
    }

    public void setArrb(NodoOrtogonal arrb) {
        this.arrb = arrb;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }     
    
}
