/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Implementacion;

import AST.Simbolo;
import java.util.LinkedList;

/**
 *
 * @author da9ni5el
 */
public class NodoA {
    private int identificador;
    private Object valor;
    private boolean anulable;
    private LinkedList<Integer> primeros;
    private LinkedList<Ultimos> ultimos;
    private LinkedList<NodoA> hijos;

    public NodoA() {
        this.identificador = -1;
        this.valor = null;
        this.anulable = false;
        this.primeros = null;
        this.ultimos = null;
        this.hijos = null;
    }

    public String getTexto() {
        StringBuffer sb = new StringBuffer();
        String lvalor = "";
        if(valor != null) {
            Simbolo s = (Simbolo)valor;
            lvalor = s.valor.toString();
            if(lvalor.contains("\"")) {
                lvalor = lvalor.replace("\"", "");
            }
            
            sb.append(lvalor);
        }
        
        sb.append("\\nAnulable: ").append(anulable);
        if(identificador != -1)
            sb.append("\\n#Nodo: ").append(identificador);
        
        return sb.toString();
    }
    
    public String escribirUltimos() {
        StringBuffer sb = new StringBuffer();
        boolean primero = true;
        for(Ultimos t: ultimos) {
            if(primero) {
                sb.append(t.getUltimo());
            } else {
                sb.append(",").append(t.getUltimo());
            }
            
            primero = false;
        }
        return sb.toString();
    }
    
    public String escribirPrimeros() {
        StringBuffer sb = new StringBuffer();
        boolean primero = true;
        for(Integer i: primeros) {
            if(primero) {
                sb.append(i);
            } else {
                sb.append(",").append(i);
            }
            
            primero = false;
        }
        return sb.toString();
    }
    
    
    
    public boolean isPNull() {
       if(primeros == null)
           return true;
       return false;
    }
    
    public boolean isUNull() {
        if(ultimos == null)
            return true;
        return false;
    }
    
    public boolean isHijosNull() {
        if(hijos ==  null)
            return true;
        return false;
    }
    
    
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public boolean isAnulable() {
        return anulable;
    }

    public void setAnulable(boolean anulable) {
        this.anulable = anulable;
    }
    
    public boolean getAnulable(){ 
        return this.anulable;
    }

    public LinkedList<Integer> getPrimeros() {
        return primeros;
    }

    public void setPrimeros(LinkedList<Integer> primeros) {
        this.primeros = primeros;
    }

    public LinkedList<Ultimos> getUltimos() {
        return ultimos;
    }

    public void setUltimos(LinkedList<Ultimos> ultimos) {
        this.ultimos = ultimos;
    }

    public LinkedList<NodoA> getHijos() {
        return hijos;
    }

    public void setHijos(LinkedList<NodoA> hijos) {
        this.hijos = hijos;
    }
    
    public void agregarHijo(NodoA h) {
        this.hijos.add(h);
    }
    
}
