/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Implementacion;


import java.util.LinkedList;
/**
 *
 * @author da9ni5el
 */
public class Ultimos {
    
    private int ultimo;
    private LinkedList<Integer> siguientes;

    public Ultimos() {
    }

    public Ultimos(int ultimo, LinkedList<Integer> siguientes) {
        this.ultimo = ultimo;
        this.siguientes = siguientes;
    }

    public boolean isSigNull() {
        if(siguientes == null)
            return true;
        return false;
    }
    
    public LinkedList<Integer> getSiguientes() {
        return siguientes;
    }

    public void setSiguientes(LinkedList<Integer> siguientes) {
        this.siguientes = siguientes;
    }

    public void setUltimo(int ultimo) {
        this.ultimo = ultimo;
    }

    public int getUltimo() {
        return ultimo;
    }
}
