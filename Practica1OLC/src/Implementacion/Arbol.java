/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Implementacion;

import AST.Nodo;
import AST.Regla;
import AST.Simbolo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author da9ni5el Se construye el arbol aqui
 */
public class Arbol {

    private LinkedList<Object> er;
    private NodoA raiz;
    private String nombre;
    private boolean error;

    public Arbol(LinkedList er, String nombre) {
        this.er = er;
        error = false;
        this.nombre = nombre;
    }

    public void generarGrafo() {
        LinkedList erlocal = (LinkedList) er.clone();
        doblar(erlocal);
        while (erlocal.size() > 1) {
            doblar(erlocal);
        }

        if (!error) {
            desdoblar((auxiliar) erlocal.get(0));
        }
    }

    private LinkedList doblar(LinkedList erlocal) {
        Simbolo s = null;
        int posact = 0;
        auxiliar aux;

        for (int i = 0; i < erlocal.size(); i++) {
            if (erlocal.get(i) instanceof auxiliar) {
                continue;
            }
            s = (Simbolo) erlocal.get(i);//se debe hacer lo mismo que abajo 
            if (s.Enum == Simbolo.EnumTipo.simbolo) { // esto representa que estoy tomando un operador
                if (operadorBinarios(s.valor)) {
                    //deberia de poder explorar dos casillas mas 
                    //Aqui mismo estaria validando la ER.
                    posact = i;
                    if ((posact + 2) <= erlocal.size() - 1) {
                        //Podria estar econtrando una reduccion.( O bien un dobles)
                        if (seDoblaBinario(erlocal.get(posact + 1), erlocal.get(posact + 2))) {
                            aux = new auxiliar(erlocal.get(posact), erlocal.get(posact + 1), erlocal.get(posact + 2));

                            erlocal.set(posact, aux); // hacer er como objeto
                            erlocal.remove((posact + 1));
                            erlocal.remove((posact + 1));
                            System.out.print("");
                        }
                    } else {
                        error = true;
                    }

                } else {
                    //deberia de poder explorar una sola casilla mas.
                    posact = i;
                    if ((posact + 1) <= erlocal.size() - 1) {
                        if (seDoblaUnario(erlocal.get(posact + 1))) {
                            aux = new auxiliar(erlocal.get(posact), erlocal.get(posact + 1), null);

                            erlocal.set(posact, aux);
                            erlocal.remove(posact + 1);
                            System.out.print("");
                        }
                    } else {
                        error = true;
                    }
                }
            }
        }

        if (error) {
            return null;
        }

        return erlocal;
    }

    private void desdoblar(auxiliar a) {
        // Aqui se empieza a crear el arbol como tal
        raiz = new NodoA();
        raiz.setValor(a);

        expander(raiz);

        NodoA temporal = new NodoA();
        temporal.setValor(new Simbolo(Simbolo.EnumTipo.cadena, "#"));

        NodoA auxiliar = raiz;
        raiz = new NodoA();
        raiz.setValor(new Simbolo(Simbolo.EnumTipo.simbolo, "."));
        raiz.setHijos(new LinkedList<>());
        raiz.agregarHijo(auxiliar);
        raiz.agregarHijo(temporal);

        //Al alcanzar este punto. Estoy seguro, que ya tengo completo el arbol
        int contador = 1;
        auxiliar aux = new auxiliar(contador, null, null);
        llenarNodos(raiz, aux);
        primeros_ultimos(raiz);
//        siguientes(raiz);
//        recorridoPostOrden(raiz);
//        graficarABB("prueba_Arbol", raiz);
    }

    private void expander(NodoA raiz) {
        if (raiz == null) {
            return;
        }

        if (raiz.getValor() instanceof auxiliar) {
            auxiliar temporal = (auxiliar) raiz.getValor();
            Simbolo operador = (Simbolo) temporal.operador;

            raiz.setValor(operador);
            if (raiz.isHijosNull()) {
                raiz.setHijos(new LinkedList<>());
            }

            if (operadorBinarios(operador.valor)) {
                NodoA t = new NodoA();
                t.setValor(temporal.operando1);
                raiz.getHijos().add(t);

                t = new NodoA();
                t.setValor(temporal.operando2);
                raiz.getHijos().add(t);
            } else {
                NodoA t = new NodoA();
                t.setValor(temporal.operando1);

                raiz.getHijos().add(t);
            }
        } else {
            return;
        }

        //Si llego a este punto, tengo que tener dos hijos. 
        expander(raiz.getHijos().get(0));
        if (raiz.getHijos().size() > 1) {
            expander(raiz.getHijos().get(1));
        }
    }

    private boolean seDoblaBinario(Object a, Object b) {
        boolean salida = false;
        Simbolo alocal = null;
        Simbolo blocal = null;

        if (a instanceof auxiliar) {
            alocal = new Simbolo(Simbolo.EnumTipo.dob, a);
        } else {
            alocal = (Simbolo) a;
        }

        if (b instanceof auxiliar) {
            blocal = new Simbolo(Simbolo.EnumTipo.dob, b);
        } else {
            blocal = (Simbolo) b;
        }

        if (alocal.Enum != Simbolo.EnumTipo.simbolo && blocal.Enum != Simbolo.EnumTipo.simbolo) {
            salida = true;
        }

        return salida;
    }

    private boolean operadorBinarios(Object valor) {
        if (valor.equals(".") || valor.equals("|")) {
            return true;
        }
        return false;
    }

    private boolean seDoblaUnario(Object valor) {
        boolean salida = false;
        Simbolo alocal = null;

        if (valor instanceof auxiliar) {
            alocal = new Simbolo(Simbolo.EnumTipo.dob, valor);
        } else {
            alocal = (Simbolo) valor;
        }

        if (alocal.Enum != Simbolo.EnumTipo.simbolo) {
            salida = true;
        }

        return salida;
    }

    //metodo del arbol inicia aqui
    private void llenarNodos(NodoA raiz, auxiliar contador) {
        if (raiz == null) {
            return;
        }

        if (!raiz.isHijosNull()) {
            llenarNodos(raiz.getHijos().get(0), contador);
            if (raiz.getHijos().size() > 1) {
                llenarNodos(raiz.getHijos().get(1), contador);
            }
        }

        Simbolo s = (Simbolo) raiz.getValor();
        if (s.Enum != Simbolo.EnumTipo.simbolo) {
            raiz.setAnulable(false);
            raiz.setIdentificador(Integer.valueOf(contador.operador.toString()));
            int temporal = Integer.valueOf(contador.operador.toString());
            contador.operador = ++temporal;
        } else {

            NodoA anulable = null;
            NodoA anulable2 = null;
            switch (s.valor.toString()) {
                case "*":
                    raiz.setAnulable(true);
                    break;
                case "?":
                    raiz.setAnulable(true);
                    break;
                case "|":
                    anulable = raiz.getHijos().get(0);
                    anulable2 = raiz.getHijos().get(1);

                    if (anulable.getAnulable() || anulable2.getAnulable()) {
                        raiz.setAnulable(true);
                    } else {
                        raiz.setAnulable(false);
                    }
                    break;
                case ".":
                    anulable = raiz.getHijos().get(0);
                    anulable2 = raiz.getHijos().get(1);
                    if (anulable.getAnulable() && anulable2.getAnulable()) {
                        raiz.setAnulable(true);
                    } else {
                        raiz.setAnulable(false);
                    }
                    break;
                case "+":
                    anulable = raiz.getHijos().get(0);
                    raiz.setAnulable(anulable.getAnulable());
                    break;
            }
        }
    }

    private void primeros_ultimos(NodoA raiz) {
        if (raiz == null) {
            return;
        }

        if (!raiz.isHijosNull()) {
            primeros_ultimos(raiz.getHijos().get(0));
            if (raiz.getHijos().size() > 1) {
                primeros_ultimos(raiz.getHijos().get(1));
            }
        }

        //Aqui se calculan los primeros y ultimos para cada nodo
        Simbolo s = (Simbolo) raiz.getValor();
        if (raiz.isPNull()) {
            raiz.setPrimeros(new LinkedList<>());
        }
        if (raiz.isUNull()) {
            raiz.setUltimos(new LinkedList<>());
        }
        if (s.Enum != Simbolo.EnumTipo.simbolo) {

            raiz.getPrimeros().add(raiz.getIdentificador());
            raiz.getUltimos().add(new Ultimos(raiz.getIdentificador(), null));
//            raiz.getUltimos().add(raiz.getIdentificador());

        } else {
            NodoA aux1 = null;
            NodoA aux2 = null;
            switch (s.valor.toString()) {

                case "*":
                    aux1 = raiz.getHijos().get(0);
                    copyListTo(raiz.getPrimeros(), aux1.getPrimeros());
                    copyListToUltimo(raiz.getUltimos(), aux1.getUltimos());
                    break;
                case "+":
                    aux1 = raiz.getHijos().get(0);
                    copyListTo(raiz.getPrimeros(), aux1.getPrimeros());
                    copyListToUltimo(raiz.getUltimos(), aux1.getUltimos());
                    break;
                case "?":
                    aux1 = raiz.getHijos().get(0);
                    copyListTo(raiz.getPrimeros(), aux1.getPrimeros());
                    copyListToUltimo(raiz.getUltimos(), aux1.getUltimos());
                    break;
                case "|":
                    aux1 = raiz.getHijos().get(0);
                    aux2 = raiz.getHijos().get(1);

                    copyListTo(raiz.getPrimeros(), aux1.getPrimeros());
                    copyListTo(raiz.getPrimeros(), aux2.getPrimeros());

                    copyListToUltimo(raiz.getUltimos(), aux1.getUltimos());
                    copyListToUltimo(raiz.getUltimos(), aux2.getUltimos());
                    break;
                case ".":
                    aux1 = raiz.getHijos().get(0);
                    aux2 = raiz.getHijos().get(1);

                    //primeros
                    if (aux1.getAnulable()) {
                        copyListTo(raiz.getPrimeros(), aux1.getPrimeros());
                        copyListTo(raiz.getPrimeros(), aux2.getPrimeros());
                    } else {
                        copyListTo(raiz.getPrimeros(), aux1.getPrimeros());
                    }

                    //ultimos
                    if (aux2.getAnulable()) {
                        copyListToUltimo(raiz.getUltimos(), aux1.getUltimos());
                        copyListToUltimo(raiz.getUltimos(), aux2.getUltimos());
                    } else {
                        copyListToUltimo(raiz.getUltimos(), aux2.getUltimos());
                    }

                    break;
            }

        }

    }

    private void siguientes(NodoA raiz) {

        if (!raiz.isHijosNull()) {
            siguientes(raiz.getHijos().get(0));
            if (raiz.getHijos().size() > 1) {
                siguientes(raiz.getHijos().get(1));
            }
        }
        
        Simbolo s = (Simbolo)raiz.getValor();
        
        if(s.Enum == Simbolo.EnumTipo.simbolo) {
            switch(s.valor.toString()) {
                case "+":
                    //sig c1 -> p c1
                    for(Ultimos u: raiz.getUltimos()) {
                        if(u.isSigNull()) {
                            u.setSiguientes(new LinkedList<>());
                        }
                        
                        for(int i: raiz.getPrimeros()) {
                            u.getSiguientes().add(i);
                        }
                    }
                    break;
                case "*":
                    //sig c1 -> p c1
                    for(Ultimos u: raiz.getUltimos()) {
                        if(u.isSigNull()) {
                            u.setSiguientes(new LinkedList<>());
                        }
                        
                        for(int i: raiz.getPrimeros()) {
                            u.getSiguientes().add(i);
                        }
                    }
                    break;
                case ".":
                    //sig ult c1 -> p c2
                    NodoA izdo = raiz.getHijos().get(0);
                    NodoA dcho = raiz.getHijos().get(1);
                    
                    for(Ultimos u: izdo.getUltimos()) {
                        if(u.isSigNull()) {
                            u.setSiguientes(new LinkedList<>());
                        }
                        
                        for(int i: dcho.getPrimeros()) {
                            u.getSiguientes().add(i);
                        }
                    }
                    break;
            }
        }
    }

    private void copyListTo(LinkedList<Integer> into, LinkedList<Integer> from) {
        for (int t : from) {
            into.add(t);
        }
    }

    private void copyListToUltimo(LinkedList<Ultimos> into, LinkedList<Ultimos> from) {
        for (Ultimos t : from) {
            into.add(t);
        }
    }

    private void recorridoPostOrden(NodoA raiz) {
        if (raiz == null) {
            return;
        }

        if (!raiz.isHijosNull()) {
            recorridoPostOrden(raiz.getHijos().get(0));
            if (raiz.getHijos().size() > 1) {
                recorridoPostOrden(raiz.getHijos().get(1));
            }
        }

        Simbolo s = (Simbolo) raiz.getValor();
        System.out.println("Valor: " + s.valor.toString());
        System.out.println("Primeros: " + raiz.escribirPrimeros());
        System.out.println("Ultimos: " + raiz.escribirUltimos());

    }
    
    private String encabezadoTS(NodoA raiz) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("struct [label=<");
        buffer.append("<TABLE ALIGN=\"CENTER\" BORDER=\"2\" ");
        buffer.append("CELLBORDER=\"1\" CELLSPACING=\"0\" ");
        buffer.append("FIXEDSIZE=\"TRUE\"> \n");

        //Encabezados
        buffer.append("<TR>\n");
        buffer.append("<TD>#i</TD>\n");
        buffer.append("<TD>Hoja</TD>\n");
        buffer.append("<TD>Siguientes</TD>\n");
        buffer.append("</TR>\n");

        buffer.append(graficarTs(raiz));

        buffer.append("</TABLE>>];\n");
        return buffer.toString();
        
    }
    
    private String graficarTs(NodoA raiz) {
        if (raiz == null) {
            return "";
        }

        StringBuffer sb = new StringBuffer();

        if (!raiz.isHijosNull()) {
            sb.append(rtablaPU(raiz.getHijos().get(0)));
            if (raiz.getHijos().size() > 1) {
                sb.append(rtablaPU(raiz.getHijos().get(1)));
            }
        }
        
        
        
        
        return sb.toString();
    }
    
    
    public void graficarArbol() {
        graficarABB(nombre, raiz);
    }

    private void graficarABB(String nombre, NodoA raiz) {
        if (raiz == null) {
            System.out.println("Arbol vacio");
            return;
        }
        try {
            try (BufferedWriter dotcode = new BufferedWriter(new FileWriter(new File(nombre + ".dot")))) {
                dotcode.write("digraph G{\ngraph[overlap=true, fontsize = 0.5];\n");
                dotcode.write("edge[color = black];\n");
                dotcode.write(clusterPU(raiz));
                dotcode.write(clusterArbol(raiz));
                dotcode.write("}");
                dotcode.close();
            }

            Runtime rt = Runtime.getRuntime();
            rt.exec("dot -Tpng " + nombre + ".dot -o " + nombre + ".png");
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private String clusterArbol(NodoA raiz) {
        StringBuffer sb = new StringBuffer();

        sb.append("subgraph clusterArbol{\n");
        sb.append("node[shape=ellipse, fontname=Helvetica, fixedsize=true, width=3, height=0.7];\n");
        sb.append(generarDot(raiz));
        sb.append("label=\"").append(nombre).append("\"\n");
        sb.append("\n}");

        return sb.toString();
    }

    private String clusterPU(NodoA raiz) {
        StringBuffer sb = new StringBuffer();

        sb.append("subgraph clusterPU{\n");
        sb.append("node [shape=plaintext];\n");
        sb.append(tablaPU(raiz));
        sb.append("\n}");

        return sb.toString();
    }

    private String generarDot(NodoA raiz) {
        StringBuffer sb = new StringBuffer();

        if (raiz == null) {
            return "";
        }

        sb.append("nodo").append(raiz.hashCode()).append("[label=\"");
        sb.append(raiz.getTexto());
        sb.append("\"];\n");

        for (int i = 0; i < raiz.getHijos().size(); i++) {
            NodoA temporal = raiz.getHijos().get(i);
            if (temporal != null) {
                sb.append("nodo").append(temporal.hashCode()).append("[label=\"");
                sb.append(temporal.getTexto());
                sb.append("\"];\n");

                //Ahora hago el enlace correspondiente.
                sb.append("nodo").append(raiz.hashCode());
                sb.append("->");
                sb.append("nodo").append(temporal.hashCode());
                sb.append("[label=\"Hijo").append(i).append("\"];\n");

                if (!temporal.isHijosNull()) {
                    sb.append(generarDot(temporal));
                }
            }
        }

        return sb.toString();
    }

    private String tablaPU(NodoA raiz) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("struct [label=<");
        buffer.append("<TABLE ALIGN=\"CENTER\" BORDER=\"2\" ");
        buffer.append("CELLBORDER=\"1\" CELLSPACING=\"0\" ");
        buffer.append("FIXEDSIZE=\"TRUE\"> \n");

        //Encabezados
        buffer.append("<TR>\n");
        buffer.append("<TD>#Nodo</TD>\n");
        buffer.append("<TD>Primeros</TD>\n");
        buffer.append("<TD>Ultimos</TD>\n");
        buffer.append("</TR>\n");

        buffer.append(rtablaPU(raiz));

        buffer.append("</TABLE>>];\n");
        return buffer.toString();
    }

    private String rtablaPU(NodoA raiz) {
        if (raiz == null) {
            return "";
        }

        StringBuffer sb = new StringBuffer();

        if (!raiz.isHijosNull()) {
            sb.append(rtablaPU(raiz.getHijos().get(0)));
            if (raiz.getHijos().size() > 1) {
                sb.append(rtablaPU(raiz.getHijos().get(1)));
            }
        }

        sb.append("<TR>\n");
        if (raiz.getIdentificador() == -1) {
            Simbolo s = (Simbolo) raiz.getValor();
            sb.append("<TD>").append(s.valor).append("</TD>");
        } else {
            sb.append("<TD>").append(raiz.getIdentificador()).append("</TD>");
        }
        sb.append("<TD>").append(raiz.escribirPrimeros()).append("</TD>");
        sb.append("<TD>").append(raiz.escribirUltimos()).append("</TD>");
        sb.append("</TR>\n");

        return sb.toString();
    }

}

class auxiliar {

    public Object operador;
    public Object operando1;
    public Object operando2;

    public auxiliar(Object operador, Object operando1, Object operando2) {
        this.operador = operador;
        this.operando1 = operando1;
        this.operando2 = operando2;
    }
}
